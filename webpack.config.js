const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');

const env = process.env.NODE_ENV;

console.log(`Configuration: ${env}`);
console.log(`__dirname: ${__dirname}`);

/* eslint-disable import/no-dynamic-require */
const dependencies = Object.keys(require(`${__dirname}/package`).dependencies);

const src = path.join(__dirname, 'src');

const config = {
  context: __dirname,

  entry: {
    app: path.join(src, 'app.jsx'),
    vendors: dependencies,
  },

  // output system
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[id].[chunkhash].js',
    // filename: '[name].js',
    path: path.join(src, '../dist/bundle'),
    publicPath: '/static/bundle/',
  },

  // resolves modules
  resolve: {
    extensions: ['.js', '.jsx'],
    // modulesDirectories: ['node_modules'],
    alias: {
      _app: path.join(src),
      // _img: path.join(src, 'assets', 'img'),
    },
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'eslint-loader',
        options: {
          emitWarning: true,
          configFile: '.eslintrc.js',
          // formatter: require("eslint-friendly-formatter"),
        },
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
            // Options in .babelrc
            options: {
              babelrc: false,
              plugins: [
                'transform-react-jsx',
                'transform-object-rest-spread',
                'transform-class-properties',
              ],
              presets: [
                ['env', {
                  // useBuiltIns: false,
                  targets: {
                    browsers: [
                      // '> 1%',
                      'last 2 versions',
                      'safari >= 7',
                      'iOS >= 9',
                      // 'Safari >= 9.1',
                      // 'iOS >= 10.3',
                      // 'Firefox >= 54',
                      // 'Edge >= 15',
                    ],
                  },
                }],
                'react',
              ],
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|woff(2)?|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loaders: ['url-loader?limit=10000&name=[name].[ext]'], // name=[name]-[hash].[ext]
      },
      {
        test: /\.(png|ico|jpg|jpeg|gif|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loaders: ['url-loader?limit=10000&name=[name].[ext]'],
      },
      {
        test: /\.css$/,
        use: env === 'production'
          ? ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader'],
          }) : ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: env === 'production'
          ? ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [{
              loader: 'css-loader',
            }, {
              loader: 'sass-loader',
            }],
          }) : ['css-loader', 'sass-loader'],
      },
      {
        test: /\.less$/,
        use: env === 'production'
          ? ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [{
              loader: 'css-loader',
            }, {
              loader: 'less-loader',
            }],
          }) : ['css-loader', 'less-loader'],
      },
    ],
  },

  // post css
  // postcss: [autoprefixer({ browsers: ['last 5 versions'] })],

  plugins: [
    // new PolyfillInjectorPlugin({
    //   polyfills: [
    //     'Promise',
    //     'Intl',
    //   ],
    //   service: 'https://scripts.mydomain.example/polyfills.min.js',
    // }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),

    new webpack.optimize.UglifyJsPlugin({
      uglifyOptions: {
        sourcemap: false,

        ie8: false,
        ecma: 6,
        mangle: {
          except: [
            '$', 'webpackJsonp',
          ],
          screw_ie8: true,
          keep_fnames: true,
        },
        output: {
          ecma: 6,
          comments: false,
          beautify: false,
          screw_ie8: true,
        },
        compress: {
          warnings: false,
          drop_console: true,
          screw_ie8: true,
        },
        warnings: false,
      },
    }),

    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     warnings: false,
    //   },
    // }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      appMountId: 'root',
    }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendors', filename: 'vendors.[chunkhash].js' }),
    new ExtractTextPlugin('[name].[chunkhash].css'),
    new AssetsPlugin({ path: path.join(__dirname, 'dist/'), filename: 'assets.json' }),
  ],
};

if (env === 'development') {
  config.devtool = 'source-map';
  config.output.filename = '[name].js';
  config.output.chunkFilename = '[id].js';

  config.plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      appMountId: 'root',
    }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendors', filename: 'vendors.js' }),
    new ExtractTextPlugin('[name].css'),
    new AssetsPlugin({ path: path.join(__dirname, 'dist/'), filename: 'assets.json' }),
  ];

  config.devServer = {
    port: 8080,
    contentBase: config.output.path,
    publicPath: config.output.publicPath,
    inline: true,
    hot: true,
    // hotOnly: true,
    progress: true,
    historyApiFallback: true,
    proxy: {
      '/api/*': 'http://localhost:8000',
    },
  };
}

module.exports = config;
