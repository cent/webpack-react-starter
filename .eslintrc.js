module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
  },
  plugins: [
  ],
  // extends: ['eslint:recommended'],
  extends: [
    'airbnb',
  ],
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 6,
    ecmaFeatures: {
      jsx: true,
    },
    sourceType: 'module',
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: 'webpack.config.js',
      },
    },
  },
  rules: {
    'func-names': ['error', 'never'],
    // 'indent': ['error', 4],
    'no-console': 0,
    // 'import/no-unresolved': 0,  // using: eslint-import-resolver-webpack
    // TODO: Remove temporary rules after this line
    'arrow-body-style': 0,
    'arrow-parens': 0,
    // 'arrow-parens': ['error', 'as-needed'],
    'import/no-extraneous-dependencies': 0,
    'react/forbid-prop-types': [2, { 'forbid': ['any', 'array'] }],
    'react/jsx-no-bind': 0,
    'react/no-array-index-key': 0,
    'react/jsx-closing-bracket-location': 0,
    'react/jsx-tag-spacing': [1, {
      "closingSlash": "never",
      "beforeSelfClosing": "allow",
      "afterOpening": "never"
    }],
    'jsx-a11y/href-no-hash': [0, "Link"],
    'jsx-a11y/anchor-has-content': 0,
    'jsx-a11y/no-noninteractive-element-interactions': 0,
    // End temporary rules
  },
};
