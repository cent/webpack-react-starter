import React from 'react';
import ReactDOM from 'react-dom';
import './app.css';

const App = () => (
  <h1>React App!</h1>
);

ReactDOM.render(<App/>, document.getElementById('root'));
